<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'nursery_a');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-)Z<{QnF+yV@5G:Y,{:g?~5}joezao)[[o$ZUio`K[Sp-R1DfPAOCIV+3Rz1T1HB');
define('SECURE_AUTH_KEY',  '$6{c< Qt2r3N:1WB~-9vhLv$?Til~z],]3rG>&5J:>pl^NRqN@l%a4IWOnW+aDfK');
define('LOGGED_IN_KEY',    'qdbbUS*]yD]6AxnS~&jk+$Z*W1S9*^@AIthoL?TUc7_6%]=;<Fi.u{mwD^MAoZ`/');
define('NONCE_KEY',        'F;U_4l ]ot8gc]S2_+Rvg/ZI9+ oZ-}[C<%6A.Tt$^p!d/J1YW_m]MrD^{B]-iNz');
define('AUTH_SALT',        '>;cFH/dY|rtT_Hr&C/~P>w|11S,DtUB.pJ;vLY1oSHh+.*dVowA>%P|tM%4x}j9)');
define('SECURE_AUTH_SALT', 'molss#6jc}}Wm0k%EN,k!+BK}.Y?x2aVV:rH[/VY,wl/A{xK`hzAX)#0(3$H}7l4');
define('LOGGED_IN_SALT',   'C5r?~<Sq<yV.[<$mcM_RqMIGtKLnx6{|J1!HO]C~SS$!3WpFG>0`D9c*hW<+}ig5');
define('NONCE_SALT',       '<Z[L%*NisU7U wF}:&Z%;T5%9s:2Jq*L=@m`jcu!og9a {6=XbPp(cA#A.G` zc:');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'nursery_a_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_MEMORY_LIMIT', '2048M');
define('DISALLOW_FILE_EDIT', true);
define('WPCF7_LOAD_CSS', false);
define('WPCF7_AUTOP', false);
define('WP_HOME', 'http://landing-a.dev');
define('WP_SITEURL', 'http://landing-a.dev');

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
