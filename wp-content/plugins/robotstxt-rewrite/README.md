# Robots.txt rewrite

Plugin provide to help search engines to indexing site correctly.

A simple plugin to manage your robots.txt. Plugin donn't create the file or edit it. This plugin edit WordPress output of robots.txt content. And get you a easy and usable interface to manage it.

## Changelog

### 1.4
*Release Date - 18th September, 2016*

* Fix PHP7 capability. Update HTML helper for admin.


### 1.3
*Release Date - 14th July, 2016*

* Bug fix `blog_public` option saving
* Add site map field to show
* Add notice if you have not saved `robots_options`

### 1.2
*Release Date - 6th June, 2016*

* Bug fix row repeater
* Add created by text in the end of robots.txt content
* Add script to open robots.txt content in new window for more usability

### 1.1
*Release Date - 4th May, 2016*

* Add robots.txt physical file checking.
* Applying this plugin options only if it was saved.
* Plugin description change.

### 1.0
*Release Date - 1st May, 2016*

* Initial
