<!doctype html>
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> prefix="http://ogp.me/ns#"> <!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#14caf0">
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/css/bundle.min.css">
    <link rel="shortcut icon" href="<?= get_stylesheet_directory_uri(); ?>/favicon.ico">
    <title><?php wp_title(); ?></title>

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?> >

    <?php // format phone number
    $formatedPhone1 = '8923' . preg_replace( '/[^0-9.\+]+/', '', get_field( 'contacts-phone' ) ); 
    $formatedPhone2 = '8923' . preg_replace( '/[^0-9.\+]+/', '', get_field( 'contacts-phone-2' ) ); 
    ?>

    <!-- start modals -->
    <!-- order modal -->
    <div id="order" class="mfp-hide modal">
      <div class="modal-close"></div><strong class="modal-title">Заполните форму</strong>
      <p class="modal-subtitle">и наш менеджер свяжется с Вами в течение <span>4 </span>минут</p>
      <?php echo do_shortcode( '[contact-form-7 id="112" html_class="modal-form"]' ); ?>
    </div>

    <!-- price order modal -->
    <div id="price-order" class="mfp-hide modal">
      <div class="modal-close"></div><strong class="modal-title">Спасибо за доверие!</strong>
      <p class="modal-subtitle">оставьте свои данные, <br>и наш менеджер свяжется с Вами в течение <span>4 </span>минут</p>
      <?php echo do_shortcode( '[contact-form-7 id="112" html_class="modal-form"]' ); ?>
    </div>

    <!-- callback modal -->
    <div id="callback" class="mfp-hide modal">
      <div class="modal-close"></div><strong class="modal-title">Закажите обратный звонок</strong>
      <p class="modal-subtitle">наш менеджер свяжется с Вами в течение <span>4 </span>минут</p>
      <?php echo do_shortcode( '[contact-form-7 id="111" html_class="modal-form"]' ); ?>
    </div>

    <!-- answer modal -->
    <div id="answer" class="mfp-hide modal">
      <div class="modal-close"></div>
      <strong class="modal-title">Заполните форму</strong>
      <p class="modal-subtitle">
        и наш менеджер свяжется с Вами в течение <span>4</span> минут, 
        <br> расскажет о нашем садике и <br>
        <span>с удовольствием ответит на все волнующие Вас вопросы</span>
      </p>
      <?php echo do_shortcode( '[contact-form-7 id="113" html_class="modal-form"]' ); ?>
    </div>

    <!-- menu modal -->
    <div id="menu" class="mfp-hide modal menu-modal">
      <div class="modal-close"></div>
      <strong class="modal-title">Пример нашего меню:</strong>
      <img src="<?= get_stylesheet_directory_uri(); ?>/img/photo-menu.png">
    </div>

    <!-- thanks modal -->
    <div id="thanks" class="mfp-hide modal thanks-modal">
      <div class="modal-close"></div>
      <strong class="modal-title">Спасибо за доверие!</strong>
      <p class="modal-subtitle">
        Наш менеджер свяжется с Вами в течение <span>4</span> минут,
        <small>а мы уже ждем Вас в гости по адресам наших садиков:</small>
      </p>
      <p class="thanks-modal-addresses">
        <small>г. Новосибирск</small>
        Войкова, 131 <br>Авиастроителей, 2/2 г
      </p>
      <span class="thanks-close thanks-modal-close">Закрыть</span>
    </div>

    <!-- sale thanks modal -->
    <div id="sale-thanks" class="mfp-hide modal thanks-modal thanks-sale-modal">
      <div class="modal-close"></div>
      <strong class="modal-title">Спасибо за доверие!</strong>
      <p class="modal-subtitle">В знак нашей благодарности мы дарим вам<strong>скидку 5000Р</strong>на первый месяц развития в нашем садике</p>
      <p class="thanks-modal-coupon">Чтобы воспользоваться скидкой, сообщите нам кодовое слово:<strong>“калейдоскоп 5”</strong></p>
      <small class="thanks-modal-phone">Наш телефон:</small>
      <a href="tel:<?= $formatedPhone1; ?>" class="thanks-modal-close"><?= get_field( 'contacts-phone' ); ?></a>
    </div>

    <!-- team modals -->
    <?php $team = get_field( 'team-list' ); ?>
    <?php for ( $i = 0; $i < count( $team ) - 1; $i++) : ?>
    <div id="team-<?= $i + 1; ?>" class="mfp-hide modal team-modal">
      <span class="modal-team-close"></span>
      <strong class="modal-team-title"><?= $team[$i]['name-full']; ?></strong>
      <p class="modal-team-text"><?= $team[$i]['text']; ?></p>
    </div>
    <?php endfor; ?>

    <!-- files modal -->
    <div id="files" class="mfp-hide modal files-modal"><span class="modal-team-close"></span>
      <div class="modal-files-slider">
        
        <?php $files = get_field( 'files-list' ); ?>
        <?php foreach ( $files as $file ) : ?>
        <img src="<?= $file['img']; ?>">
        <?php endforeach; ?>

      </div>
    </div>
    <!-- end modals -->
    
    <!-- start header -->
    <header role="banner">
      <div class="container">
        <nav class="header-menu">
          <?php wp_nav_menu( array(
              'theme_location' => 'header-menu',
              'container' => ''
          ) ); ?>
        </nav>
        <span class="header-menu-burger">
          <span></span>
          <span></span>
          <span></span>
        </span>
        <div class="header-callback">
          <a href="tel:<?= $formatedPhone1; ?>" class="header-callback-phone"><?= get_field( 'contacts-phone' ); ?></a>
          <a href="#callback" class="header-callback-btn popup">Заказать обратный звонок</a>
        </div>
      </div>
    </header>
    <!-- end header -->

    <!-- start intro -->
    <section class="block block-intro">
      <div class="container">
        <div class="intro-rockets">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-1.png" class="rocket rocket-1">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-2.png" class="rocket rocket-2">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-3.png" class="rocket rocket-3">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-4.png" class="rocket rocket-4">
          </div>
        <div class="intro-clouds">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-1">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-2">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-3">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-4">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-5">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-6">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-7">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-8">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-9">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-white.png" class="cloud cloud-10">
        </div>
        <h1 class="intro-title">Частный детский сад</h1>
        <h2 class="intro-subtitle">Калейдоскоп</h2>
        <h3 class="intro-description"><?= get_field( 'intro-title-1' ); ?></h3>
        <div class="intro-cloud-text">
          <p><?= get_field( 'intro-title-2' ); ?></p>
        </div>
        <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-childs.png" class="intro-rocket-childs">
        <p class="intro-cta-video"><span>Убедитесь в этом сами! </span>Посмотрите видео о нашем детском саде</p>
        <div class="intro-video">
          <a href="<?= get_field( 'intro-video-link' ); ?>" class="intro-video-inner">
            <img src="<?= get_stylesheet_directory_uri(); ?>/img/video-player-frame.png" class="intro-video-frame">
          </a>
        </div>
        <div class="intro-cta">
          <?php $cta = get_field( 'intro-cta' ); $cta = $cta[0]; ?>
          <strong class="intro-cta-title"><?= $cta['title']; ?></strong>
          <p class="intro-cta-subtitle"><?= $cta['subtitle']; ?></p>
          <a href="#order" class="intro-cta-btn popup">Записаться к нам на экскурсию</a>
          <small class="intro-cta-tooltip"><?= $cta['text']; ?></small>
        </div>
        <div class="intro-blue-clouds">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-blue-1.png" data-bottom-top="transform: translateY(200px);" data-top="transform: translateY(-100px);" class="cloud cloud-blue cloud-blue-1">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-blue-2.png" data-bottom-top="transform: translateY(200px);" data-top="transform: translateY(-100px);" class="cloud cloud-blue cloud-blue-2">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-blue-3.png" data-bottom-top="transform: translateY(200px);" data-top="transform: translateYX(-100px);" class="cloud cloud-blue cloud-blue-3">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-blue-4.png" data-bottom-top="transform: translateY(200px);" data-top="transform: translateY(-100px);" class="cloud cloud-blue cloud-blue-4">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/cloud-blue-5.png" data-bottom-top="transform: translateY(200px);" data-top="transform: translateY(-100px);" class="cloud cloud-blue cloud-blue-5">
        </div>
      </div>
      <div class="intro-bg-bottom"></div>
    </section>
    <!-- end intro -->
  
    <!-- start advantages -->
    <section id="advantages" class="block block-advantages">
      <div class="container">
        <h2 class="advantages-title"><?= get_field( 'advantages-title' ); ?></h2>

        <?php $advantages = get_field( 'advantages-list' ); ?>
        <div class="advantages-list">
          <div class="advantages-list-top">

            <?php for ($i = 0; $i < 4; $i++) : ?>
            <div class="advantage-single">
              <img src="<?= $advantages[$i]['img']; ?>" alt="<?= str_replace('<br>', '', $advantages[$i]['title']); ?>" class="advantage-single-icon">
              <strong class="advantage-single-title"><?= $advantages[$i]['title']; ?></strong>
              <p class="advantage-single-desc"><?= $advantages[$i]['text']; ?></p>
            </div>
            <?php endfor; ?>

          </div>
          <div class="advantages-list-bottom">
            <div class="advantages-list-bottom-left">

              <?php for ($i = 4; $i < 6; $i++) : ?>
              <div class="advantage-single">
                <img src="<?= $advantages[$i]['img']; ?>" alt="<?= str_replace('<br>', '', $advantages[$i]['title']); ?>" class="advantage-single-icon">
                <strong class="advantage-single-title"><?= $advantages[$i]['title']; ?></strong>
                <p class="advantage-single-desc">Г<?= $advantages[$i]['text']; ?></p>
              </div>
              <?php endfor; ?>

            </div>
            <img src="<?= get_stylesheet_directory_uri(); ?>/img/advantages-castle.png" class="advantages-list-bottom-castle">
            <div class="advantages-list-bottom-right">

              <?php for ( $i = 6; $i < 8; $i++ ) : ?>
              <div class="advantage-single">
                <img src="<?= $advantages[$i]['img']; ?>" alt="<?= str_replace('<br>', '', $advantages[$i]['title']); ?>" class="advantage-single-icon">
                <strong class="advantage-single-title"><?= $advantages[$i]['title']; ?></strong>
                <p class="advantage-single-desc">Г<?= $advantages[$i]['text']; ?></p>
              </div>
              <?php endfor; ?>

            </div>
          </div>
        </div>
        <div class="advantages-more">
          <strong class="advantages-more-title">ЭТО НЕВЕРОЯТНО! <span>Но еще у нас:</span></strong>
          <?php $advantagesMore = get_field( 'advantages-more-list' ); ?>
          <div class="advantages-more-list">

            <?php foreach ( $advantagesMore as $advantageMore ) : ?>
            <div class="advantage-more-single">
              <figure class="advantage-more-single-icon">
                <img src="<?= $advantageMore['img']; ?>" alt="<?= str_replace('<br>', '', $advantageMore['title']); ?>">
              </figure>
              <strong class="advantage-more-single-title"><?= $advantageMore['title']; ?></strong>
              <p class="advantage-more-single-desc"><?= $advantageMore['text']; ?></p>
            </div>
            <?php endforeach; ?>

          </div>
        </div>
      </div>
    </section>
    <!-- end advantages -->
    
    <!-- start sale -->
    <section id="sale" class="block block-sale">
      <div class="container">
        <h2 class="sale-title"><?= get_field( 'sale-title' ); ?></h2>
        <div class="sale-content">
          <div class="sale-content-title"><?= get_field( 'sale-form-title' ); ?></div>
          <div class="sale-content-form">
            <?php echo do_shortcode( '[contact-form-7 id="110" title="Акция/скидка - форма"]' ); ?>
          </div>
        </div>
      </div>
      <div class="sale-counter">
        <p class="sale-counter-title">Поторопитесь! <span>До конца действия акции осталось:</span></p>
        <div id="counter-clock" class="sale-counter-clock">
          <div class="sale-counter-clock-part sale-counter-clock-days">
            <div class="sale-counter-clock-part-bg"></div>
            <div class="sale-counter-clock-part-content">
              <p class="sale-counter-clock-part-content-num days-num">2</p>
              <p class="sale-counter-clock-part-content-desc days-desc">Дня</p>
            </div>
          </div>
          <div class="sale-counter-clock-part sale-counter-clock-hours">
            <div class="sale-counter-clock-part-bg"></div>
            <div class="sale-counter-clock-part-content">
              <p class="sale-counter-clock-part-content-num hours-num">12</p>
              <p class="sale-counter-clock-part-content-desc hours-desc">Часов</p>
            </div>
          </div>
          <div class="sale-counter-clock-part sale-counter-clock-minutes">
            <div class="sale-counter-clock-part-bg"></div>
            <div class="sale-counter-clock-part-content">
              <p class="sale-counter-clock-part-content-num minutes-num">45</p>
              <p class="sale-counter-clock-part-content-desc minutes-desc">Минут</p>
            </div>
          </div>
          <div class="sale-counter-clock-part sale-counter-clock-seconds">
            <div class="sale-counter-clock-part-bg"></div>
            <div class="sale-counter-clock-part-content">
              <p class="sale-counter-clock-part-content-num seconds-num">60</p>
              <p class="sale-counter-clock-part-content-desc seconds-desc">Секунд</p>
            </div>
          </div>
        </div><span class="sale-counter-decor-1"></span><span class="sale-counter-decor-2"></span>
      </div>
    </section>
    <!-- end sale -->
  
  <!-- start director -->
   <section class="block block-director">
      <div class="container">
        <div class="director-title"><?= get_field( 'director-title' ); ?></div>
        <div class="director-content">
          <div class="director-content-photo"><img src="<?= get_field( 'director-img' ); ?>" alt="<?= get_field( 'director-name' ); ?>"></div>
          <div class="director-content-about">
            <h3 class="director-content-about-name"><?= get_field( 'director-name' ); ?></h3>
            <p class="director-content-about-post"><?= get_field( 'director-post' ); ?></p>
            <article class="director-content-about-speech">
              <blockquote><?= get_field( 'director-text' ); ?></blockquote>
            </article>
          </div>
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/director-signature.png" class="director-content-signature">
          <a href="#order" class="director-content-btn popup">Записаться на занятия</a>
        </div>
      </div>
    </section>
    <!-- end director -->
  
    <!-- start portfolio -->
    <section id="portfolio" class="block block-portfolio">
      <div class="container">
        <div class="portfolio-rockets">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-1.png" class="rocket portfolio-rocket-1">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-2.png" class="rocket portfolio-rocket-2">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-3.png" class="rocket portfolio-rocket-3">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/rocket-4.png" class="rocket portfolio-rocket-4">
        </div>
        <h2 class="intro-title portfolio-title"><?= get_field( 'portfolio-title' ); ?></h2>
        <h3 class="intro-subtitle portfolio-subtitle"><?= get_field( 'portfolio-subtitle' ); ?></h3>
        <div class="portfolio-slider">
          
          <?php $portfolioList = get_field( 'portfolio-list' ); ?>
          <?php foreach ( $portfolioList as $portfolio ) : ?>
          <div class="portfolio-slide-single">
            <div class="portfolio-slide-single-photos intro-video">
              <div class="intro-video-inner">
                <div class="portfolio-slide-single-photos-slider">

                  <?php foreach ( $portfolio['imgs'] as $photo ) : ?>
                  <img src="<?=$photo['img']; ?>" alt="<?= $portfolio['title']; ?>">
                  <?php endforeach; ?>

                </div>
              </div>
            </div>
            <article class="portfolio-slide-single-info">
              <p class="portfolio-slide-single-info-title"><?= $portfolio['title']; ?></p>
              <p class="portfolio-slide-single-info-text"><?= $portfolio['text']; ?></p>
            </article>
          </div>
          <?php endforeach; ?>

        </div>
      </div>
    </section>
    <!-- end portfolio -->
  
    <!-- start about -->
   <section id="about" class="block block-about">
      <div class="container">
        <h2 class="about-title"><?= get_field( 'about-title' ); ?></h2>
      </div>
      <div class="about-slider">
        <div class="about-slider-blind-left"></div>
        <div class="about-slider-blind-right"></div>
        <div class="about-slider-slides">

          <?php $aboutPhotos = get_field( 'about-photos' ); ?>
          <?php foreach ( $aboutPhotos as $aboutPhoto ) : ?>
          <div class="about-slide-single"><img src="<?= $aboutPhoto['img']; ?>"></div>
          <?php endforeach; ?>

        </div>
      </div>
    </section>
    <!-- end about -->

    <!-- start clients -->
    <section id="clients" class="block block-clients">
      <div class="container">
        <h2 class="clients-title"><?= get_field( 'clients-title' ); ?></h2>
        <div class="clients-photos-slider">

          <?php $clients = get_field( 'clients-list' ); ?>
          <?php foreach ( $clients as $client ) : ?>
          <img src="<?= $client['img']; ?>" class="client-photo-slide-single" alt="<?= $client['title']; ?>">
          <?php endforeach; ?>

        </div>
        <div class="clients-slider">

          <?php foreach ( $clients as $client ) : ?>
          <div class="clients-slide-single">
            <div class="clients-slide-single-name"><?= $client['title']; ?></div>
            <article class="clients-slide-single-text">
              <p><?= $client['text']; ?></p>
            </article>
          </div>
          <?php endforeach; ?>

        </div>
      </div>
    </section>
    <!-- end clients -->
    
    <!-- start sale2 -->
    <section class="block block-sale2">
      <div class="container">
        <h2 class="sale2-title"><?= get_field( 'sale2-title' ); ?></h2>
        <div class="sale2-content">
          <div class="sale2-content-cta"><img src="<?= get_stylesheet_directory_uri(); ?>/img/sale2-cta.png" class="sale2-content-cta-img">
            <p class="sale2-content-cta-title">А ты уже <br>записался к нам?<small>Артем, 3 года</small></p>
          </div>
          <div class="sale2-content-text">
            <p class="sale2-content-text-offer"><?= get_field( 'sale2-cta-title' ); ?></p>
            <?= get_field( 'sale2-terms' ); ?>
          </div>
        </div><a href="#order" class="intro-cta-btn sale2-btn popup">Записать ребенка в садик</a>
      </div>
    </section>
    <!-- end sale2 -->

    <!-- start team -->
     <section id="team" class="block block-team">
      <div class="container">
        <h2 class="team-title"><?= get_field( 'team-title' ); ?></h2>
        <div class="team-slider">

          <?php $teamCount = count( $team ); $i = 0; ?>
          <?php foreach ( $team as $member ) : ?>
          <div class="team-slide-single">
            <figure class="team-slide-single-photo"><img src="<?= $member['img']; ?>"></figure>
            <div class="team-slide-single-info">
              <p class="team-slide-single-info-name"><?= $member['title']; ?></p>
              <p class="team-slide-single-info-line"><?= $member['post']; ?></p>
              <p class="team-slide-single-info-line"><?= $member['expirience']; ?></p>
              
              <?php if ( ++$i !== $teamCount ) : ?>
                <a href="#team-<?= $i; ?>" class="team-slide-single-info-btn popup">Познакомиться поближе</a>
              <?php endif; ?>

            </div>
          </div>
          <?php endforeach; ?>

        </div>

        <!-- start files -->
        <div class="team-files">
          <h2 class="team-files-title"><?= get_field( 'files-title' ); ?></h2>
          <div class="team-files-row team-files-row-top">

            <?php for ( $i = 0; $i < 2; $i++ ) : ?>
            <div class="team-file-single">
              <a href="#files" data-index="<?= $i; ?>" class="team-file-single-info popup">
                <div class="team-file-single-info-inner">
                  <strong class="team-file-single-info-title"><?= $files[$i]['title']; ?></strong>
                </div>
              </a>
              <img src="<?= $files[$i]['icon']; ?>" class="team-file-single-img">
            </div>
            <?php endfor; ?>

          </div>
          <div class="team-files-row team-files-row-bottom">

            <?php for ( $i = 2; $i < 6; $i++ ) : ?>
            <div class="team-file-single">
              <a href="#files" data-index="<?= $i; ?>" class="team-file-single-info popup">
                <div class="team-file-single-info-inner">
                  <strong class="team-file-single-info-title"><?= $files[$i]['title']; ?></strong>
                </div>
              </a>
              <img src="<?= $files[$i]['icon']; ?>" class="team-file-single-img">
            </div>
            <?php endfor; ?>

          </div>
        </div>
        <!-- end files -->

      </div>
    </section>
    <!-- end team -->

    <!-- start advantages2 -->
    <section class="block block-advantages2">
      <div class="container">
        <h2 class="advantages2-title"><?= get_field( 'advantages2-title' ); ?></h2>
        <div class="advantages2-list">

          <?php $advantages2 = get_field( 'advantages2-list' ); $i = 0; ?>
          <?php foreach ( $advantages2 as $advantage2 ) : $i++; ?>
          <div class="advantages2-single">
            <span class="advantages2-single-number advantages2-single-number-<?= $i; ?>" data-number="<?= $advantage2['number']; ?>">0</span>
            <p class="advantages2-single-desc"><?= $advantage2['text']; ?></p>
          </div>
          <?php endforeach; ?>

        </div>
      </div>
    </section>
    <!-- end advantages2 -->

    <!-- start faq -->
    <section class="block block-faq">
      <div class="container">
        <h2 class="faq-title"><?= get_field( 'faq-title' ); ?></h2>
        <p class="faq-subtitle"><?= get_field( 'faq-subtitle' ); ?></p>
        <div class="faq-list">

          <?php $faqList = get_field( 'faq-list' ); ?>
          <?php foreach ( $faqList as $faq ) : ?>
          <div class="faq-single">
            <div class="faq-single-question">
              <p class="faq-single-question-text"><?= $faq['question']; ?></p>
              <span class="faq-single-question-switcher">развернуть ответ</span>
            </div>
            <article class="faq-single-answer"><span>Наш ответ: </span><?= $faq['answer']; ?></article>
          </div>
          <?php endforeach; ?>

        </div>
        <a href="#answer" class="faq-btn popup">Задать свой вопрос</a>
      </div>
    </section>
    <!-- end faq -->

    <!-- start prices -->
    <section id="prices" class="block block-prices">
      <div class="container">
        <div class="prices-baloons">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/baloon-1.png" class="baloon baloon-1">
          <img src="<?= get_stylesheet_directory_uri(); ?>/img/baloon-2.png" class="baloon baloon-2">
        </div>
        <h2 class="prices-title"><?= get_field( 'prices-title' ); ?></h2>
        <div class="prices-list">
          <?php $prices = get_field( 'prices-list' ); ?>
          <div class="price-single">
            <strong class="price-single-sum"><?= $prices[0]['price']; ?></strong>
            <p class="price-single-title"><?= $prices[0]['title']; ?></p>
            <div class="price-single-info">
              <div class="price-single-info-inner">
                <ul class="price-single-info-list">
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-schedule.png" alt="Расписание посещения" class="price-single-info-icon">
                    <p class="price-single-info-desc">С 7 до 19</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-eating.png" alt="Питание" class="price-single-info-icon">
                    <p class="price-single-info-desc">Пяти<wbr>разовое питание</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-training.png" alt="Развивающие занятия" class="price-single-info-icon">
                    <p class="price-single-info-desc">Раз<wbr>вивающие занятия</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-walking.png" alt="Прогулка" class="price-single-info-icon">
                    <p class="price-single-info-desc">Прогулка</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-sleep.png" alt="Сон" class="price-single-info-icon">
                    <p class="price-single-info-desc">Сон</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-scene.png" alt="Сценические занятия" class="price-single-info-icon">
                    <p class="price-single-info-desc">Сцени<wbr>ческие занятия</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-speech.png" alt="Логопедические занятия" class="price-single-info-icon">
                    <p class="price-single-info-desc">Лого<wbr>педические занятия</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-money.png" alt="Возврат за пропуски" class="price-single-info-icon">
                    <p class="price-single-info-desc">Возврат за пропуски<small>70р за день без справки,</small><small>250р за день со справкой</small></p>
                  </li>
                </ul>
              </div>
              <a href="#price-order" class="price-single-info-btn popup">Записаться</a>
            </div>
          </div>
          <div class="price-single">
            <strong class="price-single-sum"><?= $prices[1]['price']; ?></strong>
            <p class="price-single-title"><?= $prices[1]['title']; ?></p>
            <div class="price-single-info price-single-info-highlight">
              <div class="price-single-info-inner">
                <ul class="price-single-info-list">
                  <li>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-schedule.png" alt="Расписание посещения" class="price-single-info-icon">
                    <p class="price-single-info-desc">С 7 до 19</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-eating.png" alt="Питание" class="price-single-info-icon">
                    <p class="price-single-info-desc">Пяти<wbr>разовое питание</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-training.png" alt="Развивающие занятия" class="price-single-info-icon">
                    <p class="price-single-info-desc">Раз<wbr>вивающие занятия</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-walking.png" alt="Прогулка" class="price-single-info-icon">
                    <p class="price-single-info-desc">2 прогулки в день</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-sleep.png" alt="Сон" class="price-single-info-icon">
                    <p class="price-single-info-desc">Сон</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-scene.png" alt="Сценические занятия" class="price-single-info-icon">
                    <p class="price-single-info-desc">Сцени<wbr>ческие занятия</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-speech.png" alt="Логопедические занятия" class="price-single-info-icon">
                    <p class="price-single-info-desc">Лого<wbr>педические занятия</p>
                  </li>
                  <li><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-money-white.png" alt="Возврат за пропуски" class="price-single-info-icon">
                    <p class="price-single-info-desc">Возврат за пропуски<small>750р за день без справки,</small><small>750р за день со справкой</small></p>
                  </li>
                </ul>
              </div><a href="#price-order" class="price-single-info-btn popup">Записаться</a>
            </div>
          </div>
          <div class="price-single price-single-individual">
            <strong class="price-single-sum"><?= $prices[2]['price']; ?></strong>
            <p class="price-single-title"><?= $prices[2]['title']; ?></p>
            <div class="price-single-info">
              <div class="price-single-info-inner">
                <p>Обсуждается лично</p>
                <p>Вы можете составить <br>свой график посещений <br>по дням и часам, <br>указать любые пожелания:</p>
                <figure>
                  <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-schedule.png" alt="Расписание посещения" class="price-single-info-icon">
                  <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-eating.png" alt="Питание" class="price-single-info-icon">
                  <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-training.png" alt="Развивающие занятия" class="price-single-info-icon">
                  <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-walking.png" alt="Прогулка" class="price-single-info-icon">
                  <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-sleep.png" alt="Сон" class="price-single-info-icon">
                  <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-scene.png" alt="Сценические занятия" class="price-single-info-icon">
                  <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-speech.png" alt="Логопедические занятия" class="price-single-info-icon">
                  <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-money.png" alt="Возврат за пропуски" class="price-single-info-icon">
                </figure>
              </div>
              <a href="#price-order" class="price-single-info-btn popup">Хочу обсудить</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end prices -->

    <!-- start contacts -->
    <section class="block block-contacts">
      <div class="container">
        <div class="contacts-left">
          <h2 class="contacts-title"><?= get_field( 'contacts-title' ); ?></h2>
          <div class="contacts-list">
            <a href="tel:<?= $formatedPhone1; ?>" class="contacts-phone"><?= get_field( 'contacts-phone' ); ?></a>
            <a href="tel:<?= $formatedPhone2; ?>" class="contacts-phone"><?= get_field( 'contacts-phone-2' ); ?></a>
            <a href="mailto:<?= get_field( 'contacts-email' ); ?>" class="contacts-email"><?= get_field( 'contacts-email' ); ?></a>
          </div>
          <p class="contacts-text">Позвоните или <br>напишите нам:</p>
          <div class="contacts-callback">
            <p class="contacts-callback-text">или закажите обратный звонок, <br>и мы свяжемся с Вами <br>в течение 7 минут</p>
            <a href="#callback" class="contacts-callback-btn popup">Заказать звонок</a>
          </div>
          <div class="contacts-addresses">
            <h4 class="contacts-addresses-title">Адреса наших садиков:<small>г. Новосибирск</small></h4>
            <address class="contacts-addresses-content">
              <span>Войкова, 131</span>
              <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-contacts.png" alt="Адреса наших садиков">
              <span>Авиастроителей, 2/2 г</span>
            </address>
            <nav class="contacts-addresses-social">
              <a href="<?= get_field( 'contacts-instagram' ); ?>" target="_blank">
                <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-instagram.png" alt="Инстаграм"></a>
              <a href="<?= get_field( 'contacts-vk' ); ?>" target="_blank">
                <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-vk.png" alt="Вконтакте">
              </a>
            </nav>
          </div>
        </div>
        <div class="contacts-map">
          <p class="contacts-map-title">Ждем Вас по адресам <br>наших садиков:</p>
          <div id="map-container" class="contacts-map-container">
            <div class="contacts-map-container-title">Заходите к нам <br>в гости!</div>
          </div>
        </div>
      </div>
    </section>
    <!-- end contacts -->

    <!-- start scripts -->
    <?php wp_footer(); ?>

    <script src="//api-maps.yandex.ru/2.0-stable/?load=package.standard&amp;amp;lang=ru-RU&amp;amp;"></script>
    <script src="<?= get_stylesheet_directory_uri(); ?>/js/bundle.min.js"></script>
    <script>
      <?php $deadline_date = get_field( 'sale-counter-date' ); ?>
      var deadline = new Date(Date.parse(new Date('<?= $deadline_date; ?>')));
      initializeClock('counter-clock', deadline);
    </script>
    <!-- end scripts -->

</body>