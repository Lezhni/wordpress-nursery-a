<?php

// register necessary js and css
// function styling() {
//     wp_enqueue_style( 'main', get_template_directory_uri() . '/css/bundle.min.css' );
// }
// add_action( 'wp_enqueue_scripts', 'styling' );

// register top menu
function register_menu() {
  register_nav_menu( 'header-menu', __( 'Верхнее меню' ) );
}
add_action( 'init', 'register_menu' );

// remove useless emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// remove admin bar
function remove_admin_bar() {
  show_admin_bar( false );
}
add_action( 'after_setup_theme', 'remove_admin_bar' );

// remove useless page types
function its_not_a_blog() {
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page( 'edit-tags.php?taxonomy=post_tag' );
    remove_submenu_page( 'themes.php', 'themes.php' );

    remove_menu_page( 'plugins.php' );
    remove_menu_page( 'edit.php?post_type=acf' );
    remove_menu_page( 'backup_guard_backups' );
}
add_action( 'admin_init', 'its_not_a_blog' );

// remove content editor (not uses)
function bye_bye_wysiwyg() {
    remove_post_type_support( 'page', 'editor' );
}
add_action( 'admin_init', 'bye_bye_wysiwyg' );